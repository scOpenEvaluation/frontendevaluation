
# Test cases

## Important - 
* Need to do code review
* Review the Debounce implementation
* Pagination can be tested using below test cases

### Test case 1 -

* Input value - "fo"
* EXPECTED No.of Search API invocations - 1
* EXPECTED search key - "fo"
* No.of Load more clicks - 2
* Expected Result - [
    four
    fourteen
    twenty-four
    thirty-four
    forty
    forty-one
    forty-two
    forty-three
    forty-four
    forty-five
    forty-six
    forty-seven
    forty-eight
    forty-nine
  ]
  
  ```Load more should be hidden```

  ```Total result count - 14```


### Test case 2 -

* Input value - "fn"
* EXPECTED No.of Search API invocations - 1
* EXPECTED search key - "fn"
* No.of Load more clicks - 1
* Expected Result - [
    fourteen,
    fifteen,
    twenty-four,
    twenty-five,
    forty-one,
    forty-seven,
    forty-nine,
  ]
  
  ```Load more should be hidden```

  ```Total result count - 7```



### Test case 3 -

* Input value - "fnr"
* EXPECTED No.of Search API invocations - 1
* EXPECTED search key - "fnr"
* No.of Load more clicks - 0
* Expected Result - [
    fourteen,
    twenty-four,
    forty-one,
    forty-seven,
    forty-nine,
  ]
  
  ```Load more should be hidden```

  ```Total result count - 5```


### Test case 4 -

* Input value - "tn"
* EXPECTED No.of Search API invocations - 1
* EXPECTED search key - "tn"
* No.of Load more clicks - 3
* Expected Result - [
    ten,
    thirteen,
    fourteen,
    fifteen,
    sixteen,
    seventeen,
    twenty-four,
    twenty-five,
    twenty-six,
    twenty-seven,
    twenty-eight,
    twenty-nine,
    thirty-one,
    thirty-seven,
    thirty-nine,
    forty-one,
    forty-seven,
    forty-nine,
  ]
  
  ```Load more should be hidden```

  ```Total result count - 18```


### Test case 5 -

* Input value - "fou"
* EXPECTED No.of Search API invocations - 1
* EXPECTED search key - "fou"
* No.of Load more clicks - 0
* Expected Result - [
    four,
    fourteen,
    twenty-four,
    thirty-four,
    forty-four,
  ]
  
  ```Load more should be hidden```

  ```Total result count - 5```


### Test case 6 -

* Input value - "z"
* EXPECTED No.of Search API invocations - 1
* EXPECTED search key - "z"
* No.of Load more clicks - 0
* Expected Result - [
    zero,
  ]
  
  ```Load more should be hidden```

  ```Total result count - 1```