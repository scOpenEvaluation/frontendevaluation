export default [
  {
    id: 1,
    name: "Rose",
    image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e6/Rosa_rubiginosa_1.jpg/440px-Rosa_rubiginosa_1.jpg",
    description:
      "A rose is a woody perennial flowering plant of the genus Rosa, in the family Rosaceae, or the flower it bears. There are over three hundred species and thousands of cultivars.",
    colors: "red, white, yellow"
  },
  {
    id: 2,
    name: "Tulip",
    image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9e/%D7%A6%D7%91%D7%A2%D7%95%D7%A0%D7%99%D7%9D.JPG/440px-%D7%A6%D7%91%D7%A2%D7%95%D7%A0%D7%99%D7%9D.JPG",
    description:
      "Tulips (Tulipa) form a genus of spring-blooming perennial herbaceous bulbiferous geophytes (having bulbs as storage organs).",
    colors: "yellow, white, pink"
  },
  {
    id: 3,
    name: "Sunflower",
    image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e6/Rosa_rubiginosa_1.jpg/440px-Rosa_rubiginosa_1.jpg",
    description:
      "Helianthus or sunflower is a genus of plants comprising about 70 species. Except for three species in South America, all Helianthus species are native to North America.",
    colors: "yellow"
  },
  {
    id: 4,
    name: "Poppy",
    image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Poppies_in_the_Sunset_on_Lake_Geneva.jpg/440px-Poppies_in_the_Sunset_on_Lake_Geneva.jpg",
    description:
      "A poppy is a flowering plant in the subfamily Papaveroideae of the family Papaveraceae. Poppies are herbaceous plants, often grown for their colourful flowers.",
    colors: "yellow, red"
  },
  {
    id: 5,
    name: "Lotus",
    image:
      "https://upload.wikimedia.org/wikipedia/commons/thumb/e/ed/Sacred_lotus_Nelumbo_nucifera.jpg/440px-Sacred_lotus_Nelumbo_nucifera.jpg",
    description:
      "Nelumbo nucifera, also known as Indian lotus, sacred lotus, bean of India, Egyptian bean or simply lotus, is one of two extant species of aquatic plant in the family Nelumbonaceae.",
    colors: "pink"
  }
];
